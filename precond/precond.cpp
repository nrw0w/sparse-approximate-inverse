﻿#include <iostream>
#include <omp.h>
#include <math.h>
#include <fstream>
#include "matrix/csr_matrix.h"
#include "matrix/dcsr_matrix.h"
#include "solver/bicgstab.h"
#include "solver/ilu0.h"
#include <solver/spai_fast_init.h>
#include <solver/spai_fast_iter.h>



using namespace std;

int main()
{
#ifdef __linux__ 

	std::string file = "/home/nrww/.vs/precond/f954e008-6e48-4aea-b6a4-0d32a441bcc9/src/matrix/fidapm37.mtx";
#elif _WIN32

	std::string file = "C:\\Users\\naryWoW\\source\\kursovaya\\precond\\matrix\\bcsstk18.mtx";
#endif
	

	double time = omp_get_wtime();
	csr_matrix test(file);
	cout << "time to read matrix: " << omp_get_wtime() - time << endl;
	
	double* b;
	double* refx = new double[test.getRows()];
	double* x;
	for (int i = 0; i < test.getRows(); i++)
	{
		refx[i] = ((double)rand() / RAND_MAX) * 2. - 1.;
	}
	b = test * refx;


	//fstream fout("usk_prec.txt", fstream::out | fstream::trunc);

	int max_thread = 6;
	int times = 10;
	int N_val_size = 4;
	int N_val[] = { 0,2,3,5 };

	double* time_for_thread = (double*)calloc((N_val_size + 1) * max_thread, sizeof(double));
	


	for (int th = 1; th <= max_thread; th++)
	{

		//omp_set_num_threads(th);

		//double tol_err = 0;
		//double time_bcgstab = 0;
		//int iter_bcgstab = 0;
		//double rel_err = 0;
		//for (int i = 0; i < times; i++)
		//{
		//	omp_set_num_threads(th);
		//	bicgstab sol;
		//	x = sol.allocation_solve(test, b);
		//	time_bcgstab += sol.getTime();
		//	iter_bcgstab += sol.getIterations();
		//	tol_err += sol.getError();

		//	double rel_norm = 0;
		//	double ref_norm = 0;
		//	for (int j = 0; j < test.getRows(); j++)
		//	{
		//		rel_norm += pow(refx[j] - x[j], 2);
		//		ref_norm += pow(refx[j], 2);
		//	}
		//	rel_err += sqrt(rel_norm) / sqrt(ref_norm);
		//	free(x);
		//}
		//cout << "\nbicgstab\naverage time over " << times << " attempts:\t\t" << time_bcgstab / times <<
		//	"\naverage iterations over " << times << " attempts:\t" << (double)iter_bcgstab / times <<
		//	"\naverage rel. error over " << times << " attempts:\t\t" << rel_err / times <<
		//	"\naverage tol. error over " << times << " attempts:\t\t" << tol_err / times << endl;

		//time_for_thread[N_val_size * max_thread + th - 1] = time_bcgstab / times;
		

		for (int N = 0; N < N_val_size; N++)
		{
			double tol_err_prec_bcgstab = 0.;
			double time_prec_bcgstab = 0;
			double time_prec_setup = 0;
			double time_prec_all = 0;
			int iter_prec_bcgstab = 0;
			double rel_err_prec_bcgstab = 0;
			double time_to_spmv = 0;
			double time_to_prec = 0;
			double time_to_prec_sol = 0;
			double time_to_prec_spmv = 0;
			double time_to_prec_set_decomp = 0;
			double time_to_prec_set_ilu = 0;

			for (int i = 0; i < times; i++)
			{
				bicgstab precSol;

				time = omp_get_wtime();
				spai_fast_iter prec(test, N_val[N], th);
				//spai_strict_iter prec(test, N_val[N], th);
				time_prec_setup += omp_get_wtime() - time;
				x = precSol.allocation_solve(test, b, prec);

				time_prec_all += omp_get_wtime() - time;
				time_prec_bcgstab += precSol.getTime();
				iter_prec_bcgstab += precSol.getIterations();
				time_to_prec += precSol.time_prec/ (double)precSol.getIterations();
				time_to_spmv += precSol.time_spmv/ (double)precSol.getIterations();
				time_to_prec_sol += prec.time_to_sol / (double)precSol.getIterations();
				time_to_prec_spmv += prec.time_to_spmv / (double)precSol.getIterations();
				time_to_prec_set_decomp += prec.time_to_decompose;
				time_to_prec_set_ilu += prec.time_to_ilu;
				tol_err_prec_bcgstab += precSol.getError();
				double rel_norm = 0;
				double ref_norm = 0;
				for (int j = 0; j < test.getRows(); j++)
				{
					rel_norm += pow(refx[j] - x[j], 2);
					ref_norm += pow(refx[j], 2);
				}
				rel_err_prec_bcgstab += sqrt(rel_norm) / sqrt(ref_norm);
				free(x);
				//free(testB);

				/*cout << "\nprecond bicgstab with N=" << N_temp[N]
				<< "\ntime:\t\t\t" << time_prec_all <<
					"\ntime to setup precond:\t" << time_prec_setup <<
					"\ntime to bicgstab:\t" << time_prec_bcgstab <<
					"\niterations :\t\t" << iter_prec_bcgstab <<
					"\nerror :\t\t\t" << pogr_prec_bcgstab << endl;*/

			}
			cout << "\nprecond bicgstab with N=" << N_val[N] << 
				"\naverage time over " << times << " attempts:\t\t\t\t" << time_prec_all / times <<
				"\naverage time to setup precond over " << times << " attempts:\t" << time_prec_setup / times <<
				"\naverage time to bicgstab over " << times << " attempts:\t\t" << time_prec_bcgstab / times <<
				"\naverage iterations over " << times << " attempts:\t\t\t" << (double)iter_prec_bcgstab / times <<
				"\naverage time to prec " << times << " attempts:\t\t\t\t" << time_to_prec / times <<
				"\naverage time to spmv " << times << " attempts:\t\t\t\t" << time_to_spmv / times <<
				"\naverage time to sol in prec " << times << " attempts:\t\t\t\t" << time_to_prec_sol / times <<
				"\naverage time to spmv in prec " << times << " attempts:\t\t\t\t" << time_to_prec_spmv / times <<
				"\naverage time to setup prec decomp " << times << " attempts:\t\t\t\t" << time_to_prec_set_decomp / times <<
				"\naverage time to setup prec ilu " << times << " attempts:\t\t\t\t" << time_to_prec_set_ilu / times <<
				"\naverage rel. error over " << times << " attempts:\t\t\t\t" << rel_err_prec_bcgstab / times <<
				"\naverage tol. error over " << times << " attempts:\t\t" << tol_err_prec_bcgstab / times << endl;

			//time_for_thread[N* max_thread +th-1] = (double)iter_prec_bcgstab / times;
			time_for_thread[N * max_thread + th - 1] = time_prec_all / times;
			
			
		}
	}
	for (int i = 0; i < max_thread; i++)
	{
		/*fout << i + 1 	
			<< '\t' << time_for_thread[0 * max_thread] / time_for_thread[0 * max_thread + i]
			<< '\t' << time_for_thread[1 * max_thread] / time_for_thread[1 * max_thread + i]
			<< '\t' << time_for_thread[2 * max_thread] / time_for_thread[2 * max_thread + i] 
			<< '\t' << time_for_thread[3 * max_thread] / time_for_thread[3 * max_thread + i] << endl;*/

		/*fout << i + 1 << '\t' << time_for_thread[0 * max_thread + i]
			<< '\t' << time_for_thread[1 * max_thread + i]
			<< '\t' << time_for_thread[2 * max_thread + i]
			<< '\t' << time_for_thread[3 * max_thread + i] << endl;*/
	}


	delete[] b;
	delete[] refx;
	free(time_for_thread);
	//fout.close();
	

	return 0;
}
