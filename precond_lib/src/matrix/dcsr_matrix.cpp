#include "matrix/dcsr_matrix.h"


dcsr_matrix::dcsr_matrix()
{
}


dcsr_matrix::dcsr_matrix(int n_row, int n_col, int n_blocks)
{
	this->n_col = n_col;
	this->n_row = n_row;
	this->n_blocks = n_blocks;

	//dyn array
	//matrix = new csr_matrix[n_blocks];

	//vector
	matrix = std::vector<csr_matrix>(n_blocks);
}

dcsr_matrix::dcsr_matrix(const dcsr_matrix& rhs)
{
	this->n_col = rhs.n_col;
	this->n_row = rhs.n_row;
	this->n_blocks = rhs.n_blocks;
	//dyn array
	/*matrix = new csr_matrix[n_blocks];
	for (int i = 0; i < n_blocks; i++)
	{
		matrix[i] = rhs.matrix[i];
	}*/

	//vector
	matrix = std::vector<csr_matrix>(rhs.matrix);

}

dcsr_matrix::dcsr_matrix(const csr_matrix input_matrix, int n_blocks) : dcsr_matrix(input_matrix.n_row, input_matrix.n_col, n_blocks)
{
	//matrix = std::vector<csr_matrix>(n_blocks);

	int remainder = n_row % n_blocks;
	int n_chunk = n_row / n_blocks;

	int* first_elem_in_inp_matrix = (int*)calloc(n_blocks + 1, sizeof(int));
	int* size_of_block = (int*)malloc(n_blocks * sizeof(int));

	for (int i = 1; i < n_blocks + 1; i++)
	{
		if (remainder > 0)
		{
			size_of_block[i - 1] = n_chunk + 1;
			first_elem_in_inp_matrix[i] = first_elem_in_inp_matrix[i - 1] + n_chunk + 1;
			remainder--;
		}
		else
		{
			size_of_block[i - 1] = n_chunk;
			first_elem_in_inp_matrix[i] = first_elem_in_inp_matrix[i - 1] + n_chunk;
		}
	}
#pragma omp parallel for schedule(static,1)
	for (int n = 0; n < n_blocks; n++)
	{


		int start_local = first_elem_in_inp_matrix[n];
		int end_local = first_elem_in_inp_matrix[n + 1];
		int n_nnz_max = input_matrix.row_index[end_local] - input_matrix.row_index[start_local];
		matrix[n] = csr_matrix(n_nnz_max, size_of_block[n], size_of_block[n]);

		int n_nnz_local = 0;
		for (int i = start_local; i < end_local; i++)
		{
			int ja = input_matrix.row_index[i];
			int jb = input_matrix.row_index[i + 1];

			//#pragma omp simd
			for (int j = ja; j < jb; j++)
			{
				if (input_matrix.col_index[j] >= start_local && input_matrix.col_index[j] < end_local)
				{
					matrix[n].val[n_nnz_local] = input_matrix.val[j];
					matrix[n].col_index[n_nnz_local] = input_matrix.col_index[j] - start_local;
					n_nnz_local++;
				}
			}
			matrix[n].row_index[i - start_local + 1] = n_nnz_local;
		}
		if (n_nnz_local != matrix[n].n_nnz)
		{
			matrix[n].n_nnz = n_nnz_local;
			matrix[n].val = (double*)realloc(matrix[n].val, matrix[n].n_nnz * sizeof(double));
			matrix[n].col_index = (int*)realloc(matrix[n].col_index, matrix[n].n_nnz * sizeof(int));
		}
	}
	free(first_elem_in_inp_matrix);
	free(size_of_block);

}

dcsr_matrix::~dcsr_matrix()
{
	//dyn array
	//if (matrix != nullptr) delete[] matrix;
}

dcsr_matrix dcsr_matrix::operator*(const dcsr_matrix& rhs) const
{
	if (this->n_blocks != rhs.n_blocks)
	{
#ifdef __linux__ 
		throw std::exception();
#elif _WIN32
		throw std::exception("Matrixs are inconsistent");
#endif
	}
	dcsr_matrix rez(rhs.n_row, rhs.n_col, rhs.n_blocks);
	//rez.matrix = std::vector<csr_matrix>(n_blocks);


#pragma omp parallel for schedule(static, 1)
	for (int i = 0; i < n_blocks; i++)
	{
		rez.matrix[i] = this->matrix[i] * rhs.matrix[i];
		//�������������� �������, ���������� �� ����� �����������
		rez.matrix[i].transpose();
		rez.matrix[i].transpose();
	}

	return rez;
}

//potential error
dcsr_matrix dcsr_matrix::operator=(const dcsr_matrix& rhs)
{
	if (this == &rhs) return *this;

	n_col = rhs.n_col;
	n_row = rhs.n_row;
	n_blocks = rhs.n_blocks;

	//dyn array
	/*if (matrix == nullptr) matrix = new csr_matrix[n_blocks];
	for (int i = 0; i < n_blocks; i++)
	{
		matrix[i] = rhs.matrix[i];
	}*/
	
	//vector
	matrix = std::vector<csr_matrix>(rhs.matrix);

	return *this;
}

void dcsr_matrix::spmv(const double* rhs, double* out)
{
	int* shift = (int*)malloc(n_blocks * sizeof(int));

	shift[0] = 0;

	for (int i = 1; i < n_blocks; i++)
	{
		shift[i] = shift[i - 1] + matrix[i - 1].n_row;
	}

	#pragma omp parallel for schedule(static, 1)
	for (int i = 0; i < n_blocks; i++)
	{
		matrix[i].spmv(rhs, out + shift[i]);
	}
	free(shift);
}

csr_matrix dcsr_matrix::to_csr() const
{
	int n_nnz = 0;
	int* shift = (int*)malloc(n_blocks * sizeof(int));

	n_nnz += matrix[0].n_nnz;
	shift[0] = 0;
	for (int i = 1; i < n_blocks; i++)
	{
		shift[i] = shift[i - 1] + matrix[i - 1].n_row;
		n_nnz += matrix[i].n_nnz;
	}
	csr_matrix rez = csr_matrix(n_nnz, n_row, n_col);
	int n_row_temp = 0;
	n_nnz = 0;
	for (int i = 0; i < n_blocks; i++)
	{
		const csr_matrix& mat = matrix[i];
		for (int j = 0; j < mat.n_row; j++)
		{
			for (int k = mat.row_index[j]; k < mat.row_index[j + 1]; k++)
			{
				rez.val[n_nnz] = mat.val[k];
				rez.col_index[n_nnz] = mat.col_index[k] + shift[i];
				n_nnz++;
			}
			rez.row_index[j + n_row_temp + 1] = n_nnz;
		}
		n_row_temp += mat.n_row;
	}
	return rez;
}


