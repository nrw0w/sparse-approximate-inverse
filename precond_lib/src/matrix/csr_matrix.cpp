#include "matrix/csr_matrix.h"

csr_matrix::csr_matrix()
{
}

csr_matrix::csr_matrix(const csr_matrix& A)
{
	this->init_csr(A.n_row, A.n_col, A.n_nnz);
	if (A.col_index == nullptr || A.row_index == nullptr || A.val == nullptr) return;
	memcpy(this->col_index, A.col_index, A.n_nnz * sizeof(int));
	memcpy(this->row_index, A.row_index, (A.n_row + 1) * sizeof(int));
	memcpy(this->val, A.val, A.n_nnz * sizeof(double));
}

csr_matrix::csr_matrix(const double* array, const int n_row, const int n_col)
{
	//number of nonzero element of a sparce matrix (n,n) of order n
	const int sparce_koef = 3;
	const int allocation_chunk = n_col >= n_row ? sparce_koef * n_col : sparce_koef * n_row;

	//rezult CSR
	init_csr(n_row, n_col, allocation_chunk);
	int n_val_size = allocation_chunk;

	for (int i = 0; i < n_row; i++)
	{
		for (int j = 0; j < n_col; j++)
		{
			if (array[i * n_col + j] == 0) continue;

			if (this->n_nnz > n_val_size)
			{
				n_val_size += allocation_chunk;
				this->val = (double*)realloc(this->val, n_val_size * sizeof(double));
				this->col_index = (int*)realloc(this->col_index, n_val_size * sizeof(int));
			}

			this->n_nnz++;
			this->val[this->n_nnz - 1] = array[i * n_col + j];
			this->col_index[this->n_nnz - 1] = j;

		}
		this->row_index[i + 1] = this->n_nnz;
	}

	this->val = (double*)realloc(this->val, this->n_nnz * sizeof(double));
	this->col_index = (int*)realloc(this->col_index, this->n_nnz * sizeof(int));

}

csr_matrix::csr_matrix(std::string filename)
{
	std::fstream file;
	file.open(filename, std::ios_base::in);

	//add header parsing
	std::string header;
	std::getline(file, header);
	while (header[0] == '%')
	{
		std::getline(file, header);
	}
	std::stringstream in(header);

	int n, m, nnz;
	in >> m >> n >> nnz;

	init_csr(n, m, nnz);
	memset(this->row_index, 0, (this->n_row + 1) * sizeof(int));

	for (int i = 0; i < nnz; i++)
	{
		int row, col;
		file >> row >> col >> this->val[i];
		this->col_index[i] = row - 1;
		this->row_index[col]++;
	}
	for (int i = 2; i < n_row + 1; i++)
	{
		row_index[i] += row_index[i - 1];
	}
	this->transpose();

	file.close();
}

csr_matrix::csr_matrix(int n_nnz, int n_row, int n_col)
{
	this->init_csr(n_row, n_col, n_nnz);
}

csr_matrix::~csr_matrix()
{
	if (col_index != nullptr) free(col_index);
	if (row_index != nullptr) free(row_index);
	if (val != nullptr) free(val);
}

void csr_matrix::init_csr(const int n_row, const int n_col, const int allocation_chunk)
{

	this->n_row = n_row;
	this->n_col = n_col;
	this->n_nnz = allocation_chunk;

	this->row_index = (int*)malloc((n_row + 1) * sizeof(int));
	this->row_index[0] = 0;

	this->val = (double*)malloc(allocation_chunk * sizeof(double));
	this->col_index = (int*)malloc(allocation_chunk * sizeof(int));

}

double* csr_matrix::operator* (const double* vector) const
{
	double* rez = (double*)calloc(this->n_row, sizeof(double));
	spmv(vector, rez);
	return rez;
}
void csr_matrix::spmv(const double* vector, double* out) const
{
	memset(out, 0, this->n_row * sizeof(double));
	for (int i = 0; i < this->n_row; ++i)
	{
//#pragma omp simd

		for (int j = this->row_index[i]; j < this->row_index[i + 1]; ++j)
		{
			out[i] += this->val[j] * vector[this->col_index[j]];
		}
	}

}
void csr_matrix::parallel_spmv(const double* vector, double* out) const
{
	memset(out, 0, this->n_row * sizeof(double));
	//nt chunk = this->n_row / omp_get_num_threads() /35;
	//b32
//#pragma omp parallel for schedule(auto) 
#pragma omp parallel for schedule(guided) 
	for (int i = 0; i < this->n_row; ++i)
	{
//#pragma omp simd
		for (int j = this->row_index[i]; j < this->row_index[i + 1]; ++j)
		{
			out[i] += this->val[j] * vector[this->col_index[j]];
		}
	}

}

csr_matrix csr_matrix::operator*(const double scalar) const
{
	csr_matrix rez = csr_matrix(*this);

	for (int i = 0; i < n_nnz; i++)
	{
		rez.val[i] *= scalar;
	}
	return rez;

}

csr_matrix csr_matrix::operator*(const int scalar) const
{
	csr_matrix rez = csr_matrix(*this);

	for (int i = 0; i < n_nnz; i++)
	{
		rez.val[i] *= scalar;
	}
	return rez;
}

csr_matrix csr_matrix::operator*(const csr_matrix& matrix) const
{
	const csr_matrix& a = *this;
	const csr_matrix& b = matrix;

	int n_c_col = b.n_col;
	int q = a.n_col;
	int n_c_row = a.n_row;

	if (a.n_col != b.n_row)
	{
#ifdef __linux__ 
		throw std::exception();
#elif _WIN32
		throw std::exception("Matrixs are inconsistent");
#endif

	}

	int* c_symb_tmp = (int*)malloc(n_c_col * sizeof(int));
	double* c_val_tmp = (double*)malloc(n_c_col * sizeof(double));

	//correct this
	int allocated_nnz = a.n_nnz + b.n_nnz;
	int allocation_chunk = allocated_nnz / 2;

	int* c_row_index = new int[n_c_row + 1];
	int* c_col_index = (int*)malloc(allocated_nnz * sizeof(int));
	double* c_val = (double*)malloc(allocated_nnz * sizeof(double));

	int n_c_nnz = 0;

	memset(c_symb_tmp, -1, n_c_col * sizeof(int));

	for (int i = 0; i < n_c_row; i++)
	{
		c_row_index[i] = n_c_nnz;
		for (int jp = a.row_index[i]; jp < a.row_index[i + 1]; jp++)
		{
			int j = a.col_index[jp];
			for (int kp = b.row_index[j]; kp < b.row_index[j + 1]; kp++)
			{
				int k = b.col_index[kp];
				if (c_symb_tmp[k] != i)
				{
					c_col_index[n_c_nnz] = k;
					n_c_nnz++;
					c_symb_tmp[k] = i;
					c_val_tmp[k] = a.val[jp] * b.val[kp];
				}
				else
				{
					c_val_tmp[k] += a.val[jp] * b.val[kp];
				}
			}

		}
		if (n_c_nnz > allocated_nnz - n_c_col)
		{
			allocated_nnz += allocation_chunk;
			c_col_index = (int*)realloc(c_col_index, allocated_nnz * sizeof(int));
			c_val = (double*)realloc(c_val, allocated_nnz * sizeof(double));
		}
		for (int vp = c_row_index[i]; vp < n_c_nnz; vp++)
		{
			int v = c_col_index[vp];
			c_val[vp] = c_val_tmp[v];
		}
	}

	c_row_index[n_c_row] = n_c_nnz;

	if (n_c_nnz != allocated_nnz)
	{
		c_col_index = (int*)realloc(c_col_index, n_c_nnz * sizeof(int));
		c_val = (double*)realloc(c_val, n_c_nnz * sizeof(double));
	}

	free(c_symb_tmp);
	free(c_val_tmp);

	csr_matrix c_rez = csr_matrix();
	c_rez.n_row = n_c_row;
	c_rez.n_col = n_c_col;
	c_rez.n_nnz = n_c_nnz;
	c_rez.col_index = c_col_index;
	c_rez.row_index = c_row_index;
	c_rez.val = c_val;

	return c_rez;

}

csr_matrix csr_matrix::operator-(const csr_matrix& matrix) const
{
	const csr_matrix& a = *this;
	const csr_matrix& b = matrix;

	if (a.n_col != b.n_col || a.n_row != b.n_row)
	{
#ifdef __linux__ 
		throw std::exception();
#elif _WIN32
		throw std::exception("Matrixs are inconsistent");
#endif
	}
	//correct this?
	int allocated_nnz = a.n_nnz + b.n_nnz;

	csr_matrix c = csr_matrix(allocated_nnz, a.n_row, a.n_col);

	int n_nnz_local = 0;
	for (int i = 0; i < c.n_row; i++)
	{
		int a_ind = a.row_index[i];
		int b_ind = b.row_index[i];
		while ((a_ind < a.row_index[i + 1]) && (b_ind < b.row_index[i + 1]))
		{
			if (a.col_index[a_ind] == b.col_index[b_ind])
			{
				//�������� �� ������ ��������� ���������
				//if (abs(a.val[a_ind] - b.val[b_ind]) < std::numeric_limits<double>::epsilon())
				if (abs(a.val[a_ind] - b.val[b_ind]) <1e-7)
				{
					a_ind++;
					b_ind++;
					continue;
				}
				c.val[n_nnz_local] = a.val[a_ind] - b.val[b_ind];
				c.col_index[n_nnz_local] = a.col_index[a_ind];
				n_nnz_local++;
				a_ind++;
				b_ind++;

			}
			else if (a.col_index[a_ind] < b.col_index[b_ind])
			{
				c.val[n_nnz_local] = a.val[a_ind];
				c.col_index[n_nnz_local] = a.col_index[a_ind];
				n_nnz_local++;
				a_ind++;

			}
			else
			{
				c.val[n_nnz_local] = -b.val[b_ind];
				c.col_index[n_nnz_local] = b.col_index[b_ind];
				n_nnz_local++;
				b_ind++;
			}
		}
		if (a_ind < a.row_index[i + 1])
		{
			for (int j = a_ind; j < a.row_index[i + 1]; j++)
			{
				c.val[n_nnz_local] = a.val[j];
				c.col_index[n_nnz_local] = a.col_index[j];
				n_nnz_local++;
			}

		}
		else if (b_ind < b.row_index[i + 1])
		{
			for (int j = b_ind; j < b.row_index[i + 1]; j++)
			{
				c.val[n_nnz_local] = -b.val[j];
				c.col_index[n_nnz_local] = b.col_index[j];
				n_nnz_local++;
			}
		}
		c.row_index[i + 1] = n_nnz_local;
	}
	if (n_nnz_local != n_nnz)
	{
		c.n_nnz = n_nnz_local;
		c.val = (double*)realloc(c.val, n_nnz_local * sizeof(double));
		c.col_index = (int*)realloc(c.col_index, n_nnz_local * sizeof(int));
	}


	/*int* c_row_index = (int*)calloc(n_row + 1, sizeof(int));
	int* c_col_index = (int*)malloc((a.n_nnz + b.n_nnz) * sizeof(int));

	//symbolic part
	int* c_sym_tmp = (int*)calloc(n_col, sizeof(int));

	int n_c_nnz = 0;
	for (int i = 0; i < n_row; i++)
	{
		c_row_index[i] = n_c_nnz;
		for (int jp = a.row_index[i]; jp < a.row_index[i + 1]; jp++)
		{
			int j = a.col_index[jp];
			c_col_index[n_c_nnz] = j;
			n_c_nnz++;
			c_sym_tmp[j] = i;
		}
		for (int jp = b.row_index[i]; jp < b.row_index[i + 1]; jp++)
		{
			int j = b.col_index[jp];
			if (c_sym_tmp[j] == i) continue;
			c_col_index[n_c_nnz] = j;
			n_c_nnz++;
		}
	}
	c_row_index[n_row] = n_c_nnz;

	free(c_sym_tmp);

	if (n_c_nnz != (a.n_nnz + b.n_nnz))
	{
		c_col_index = (int*)realloc(c_col_index, n_c_nnz * sizeof(int));
	}
	double* c_val = (double*)malloc(n_c_nnz * sizeof(double));

	//numeric part
	double* c_val_tmp = (double*)calloc(n_col, sizeof(double));

	for (int i = 0; i < n_row; i++)
	{
		for (int jp = c_row_index[i]; jp < c_row_index[i + 1]; jp++)
		{
			c_val_tmp[c_col_index[jp]] = 0;
		}

		for (int jp = a.row_index[i]; jp < a.row_index[i + 1]; jp++)
		{
			c_val_tmp[a.col_index[jp]] = a.val[jp];
		}

		for (int jp = b.row_index[i]; jp < b.row_index[i + 1]; jp++)
		{
			int j = b.col_index[jp];
			c_val_tmp[j] += b.val[jp];
		}

		for (int jp = c_row_index[i]; jp < c_row_index[i + 1]; jp++)
		{
			c_val[jp] = c_val_tmp[c_col_index[jp]];
		}
	}
	csr_matrix c_rez = csr_matrix();
	c_rez.n_col = n_col;
	c_rez.n_row = n_row;
	c_rez.n_nnz = n_c_nnz;
	c_rez.col_index = c_col_index;
	c_rez.row_index = c_row_index;
	c_rez.val = c_val;

	free(c_val_tmp);
	*/
	return c;
}

csr_matrix csr_matrix::operator=(const csr_matrix& matrix)
{
	

	if (this == &matrix || matrix.col_index == nullptr || matrix.row_index == nullptr || matrix.val == nullptr) return *this;

	if (n_nnz != matrix.n_nnz || col_index == nullptr)
	{
		col_index = (int*)realloc(col_index, matrix.n_nnz * sizeof(int));
	}
	memcpy(col_index, matrix.col_index, matrix.n_nnz * sizeof(int));

	if (n_nnz != matrix.n_nnz || val == nullptr)
	{
		val = (double*)realloc(val, matrix.n_nnz * sizeof(double));
	}
	memcpy(val, matrix.val, matrix.n_nnz * sizeof(double));

	if (n_row != matrix.n_row || row_index == nullptr)
	{
		row_index = (int*)realloc(row_index, (matrix.n_row + 1) * sizeof(int));
	}
	memcpy(row_index, matrix.row_index, (matrix.n_row + 1) * sizeof(int));

	n_nnz = matrix.n_nnz;
	n_col = matrix.n_col;
	n_row = matrix.n_row;

	return *this;
}

void csr_matrix::transpose()
{
	int n_at_col = n_row;
	int n_at_row = n_col;

	int* at_row_index = (int*)calloc(n_col + 1, sizeof(int));
	int* at_col_index = (int*)malloc(n_nnz * sizeof(int));
	double* at_val = (double*)malloc(n_nnz * sizeof(double));

	for (int i = 0; i < row_index[n_row]; i++)
	{
		if (col_index[i] + 1 < n_col) at_row_index[col_index[i] + 2]++;
	}
	if (n_col != 1)
	{
		at_row_index[0] = 0;
		at_row_index[1] = 0;
		for (int i = 2; i < n_col + 1; i++)
		{
			at_row_index[i] = at_row_index[i] + at_row_index[i - 1];
		}
	}

	for (int i = 0; i < n_row; i++)
	{

		if (row_index[i + 1] <= row_index[i]) continue;
		for (int jp = row_index[i]; jp < row_index[i + 1]; jp++)
		{
			int j = col_index[jp] + 1;
			int k = at_row_index[j];
			at_col_index[k] = i;
			at_val[k] = val[jp];
			at_row_index[j]++;
		}
	}

	free(this->row_index);
	free(this->col_index);
	free(this->val);
	this->row_index = at_row_index;
	this->col_index = at_col_index;
	this->val = at_val;
	this->n_col = n_at_col;
	this->n_row = n_at_row;

}




double* csr_matrix::allocate_convert_to_array() const
{
	double* rez = (double*)calloc(this->n_row * this->n_col, sizeof(double));

	for (int i = 0; i < this->n_row; i++)
	{
		for (int j = this->row_index[i]; j < this->row_index[i + 1]; j++)
		{
			rez[i * this->n_col + this->col_index[j]] = this->val[j];
		}
	}

	return rez;
}

int comp(const void* x, const void* y)
{
	return *(int*)x - *(int*)y;
}

void csr_matrix::random_generate(const int n_row, const int n_col, const int n_nnz_in_row)
{
	srand(time(0));

	init_csr(n_row, n_col, n_col * n_nnz_in_row);

	this->n_nnz = n_col * n_nnz_in_row;

	for (int i = 0; i < n_row; i++)
	{
		this->col_index[i * n_nnz_in_row] = i;

		for (int j = n_col - n_nnz_in_row + 1; j < n_col; j++)
		{
			int elem = rand() % (j + 1);
			bool isContain = false;
			for (int k = 0; k < j - (n_col - n_nnz_in_row); k++)
			{
				if (this->col_index[i * n_nnz_in_row + k] == elem)
				{
					isContain = true;
					break;
				}
			}
			this->col_index[i * n_nnz_in_row + j - (n_col - n_nnz_in_row)] = isContain ? j : elem;
		}

		qsort(&this->col_index[i * n_nnz_in_row], n_nnz_in_row, sizeof(int), comp);

		for (int j = 0; j < n_nnz_in_row; j++)
		{
			this->val[i * n_nnz_in_row + j] = (double)rand() / (RAND_MAX / 2) - 1.;
		}

		this->row_index[i + 1] = this->row_index[i] + n_nnz_in_row;
	}

}
