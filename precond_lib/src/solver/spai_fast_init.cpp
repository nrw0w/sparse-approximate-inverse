#include "solver/spai_fast_init.h"


void spai_fast_init::diogonal_block_decompose(const csr_matrix& input_matrix)
{
	p = dcsr_matrix(input_matrix, n_threads);
	a = input_matrix;
}
spai_fast_init::spai_fast_init(const csr_matrix& inp_matrix, bool prec) : spai_base(inp_matrix, prec)
{

}

spai_fast_init::spai_fast_init(const csr_matrix& inp_matrix, int n_iterations, int n_threads) : spai_base(inp_matrix, n_iterations, n_threads)
{
	//��� N>31 ������������ int32 � ������
	if (n_iterations >= 32)
	{
#ifdef __linux__ 
		throw std::exception();
#elif _WIN32
		throw std::exception("Not supported");
#endif
	}

	double time = omp_get_wtime();
	diogonal_block_decompose(inp_matrix);
	time_to_decompose = omp_get_wtime() - time;

	time = omp_get_wtime();
	p = ilu0::decompose(p);
	time_to_ilu = omp_get_wtime() - time;

	int n = n_iterations + 1;
	int k = n / 2 + 1;

	bcr_cache = (int*)malloc(k* sizeof(int));
	bcr_cache[0] = 1;
	for (int i = 1; i < k; i++)
	{
		bcr_cache[i] = bcr_cache[i - 1] * (n - i + 1) / i;
	}
}
spai_fast_init::~spai_fast_init()
{
	if (bcr_cache != nullptr)	free(bcr_cache);
	
}
void spai_fast_init::solve(const double* b, double* out)
{
	int n_row = p.getRows();
	//�������
	if (!prec)
	{
		memcpy(out, b, n_row * sizeof(double));
		return;
	}

	memset(out, 0, n_row * sizeof(double));

	double* temp_sol = (double*)malloc(n_row * sizeof(double));
	double* temp_mult = (double*)malloc(n_row * sizeof(double));
	memcpy(temp_mult, b, n_row * sizeof(double));

	int sign = 1;

	for (int i = 0; i <= n_iterations; i++)
	{
		ilu0::solve(p, temp_mult, temp_sol);
		for (int j = 0; j < n_row; j++)
		{
			out[j] += sign * bcr_cache[(i+1) > (n_iterations+1) / 2? n_iterations - i:i+1] * temp_sol[j];
		}
		if (i == n_iterations)
		{
			break;
		}
		a.parallel_spmv(temp_sol, temp_mult);
		sign *= -1;
	}
	free(temp_sol);
	free(temp_mult);

}