#include "solver/spai_base.h"

void spai_base::setIterations(int n_iterations)
{
	if (n_iterations < 0)
	{
		throw std::exception();

	}
	this->n_iterations = n_iterations;
}

int spai_base::getIterations()
{
	return this->n_iterations;
}

void spai_base::setThreads(int n_threads)
{
	if (n_threads < 1)
	{
		throw std::exception();

	}
	this->n_threads = n_threads;
}

int spai_base::getThreads()
{
	return n_threads;
}
//�������
spai_base::spai_base(const csr_matrix& inp_matrix, bool prec)
{
	p = dcsr_matrix(inp_matrix, 1);
	this->prec = false;
}

spai_base::spai_base(const csr_matrix& inp_matrix, int n_iterations, int n_threads)
{
	prec = true;
	this->n_iterations = n_iterations;
	this->n_threads = n_threads;
	omp_set_num_threads(n_threads);
	
}

double* spai_base::allocation_solve(const double* b)
{
	int n_row = p.getRows();
	double* rez = (double*)calloc(n_row, sizeof(double));
	solve(b, rez);
	return rez;

}