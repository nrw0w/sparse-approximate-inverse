#include "solver/ilu0.h"



csr_matrix ilu0::decompose(const csr_matrix& matrix)
{

	csr_matrix rez(matrix);

	int* iw = (int*)calloc(matrix.n_col, sizeof(int));
	int* diag_index = (int*)malloc(matrix.n_col * sizeof(int));

	int j_curr;

	for (int i = 0; i < matrix.n_row; i++)
	{
		int j1 = matrix.row_index[i];
		int j2 = matrix.row_index[i + 1];
		for (int j = j1; j < j2; j++)
		{
			iw[matrix.col_index[j]] = j;
		}
		int j;
		for (j = j1; (j < j2) && (matrix.col_index[j] < i); j++)
		{
			j_curr = matrix.col_index[j];
			double temp = rez.val[j] / rez.val[diag_index[j_curr]];
			rez.val[j] = temp;
			for (int k = diag_index[j_curr] + 1; k < matrix.row_index[j_curr + 1]; k++)
			{
				int jw = iw[matrix.col_index[k]];
				if (jw != 0)
				{
					rez.val[jw] -= temp * rez.val[k];
				}
			}

		}
		j_curr = matrix.col_index[j];
		diag_index[i] = j;
		if ((j_curr != i) || abs(rez.val[j]) < std::numeric_limits<double>::epsilon())
		{
			break;
		}
		for (int j = j1; j < j2; j++)
		{
			iw[matrix.col_index[j]] = 0;
		}

	}

	free(iw);
	free(diag_index);
	return rez;
}


dcsr_matrix ilu0::decompose(const dcsr_matrix& matrix)
{
	int n_thread = matrix.n_blocks;

	dcsr_matrix rez = dcsr_matrix(matrix.n_row, matrix.n_col, matrix.n_blocks);

#pragma omp parallel for schedule(static, 1)
	for (int i = 0; i < n_thread; i++)
	{
		rez.matrix[i] = decompose(matrix.matrix[i]);
	}

	return rez;

}

void ilu0::solve(const csr_matrix& matrix, const double* b, double* out)
{
	memcpy(out, b, matrix.n_col * sizeof(double));

	for (int i = 0; i < matrix.n_row; i++)
	{
		int ja = matrix.row_index[i];
		int jb = matrix.row_index[i + 1];
		for (int j = ja; j < jb; j++)
		{
			int col_temp = matrix.col_index[j];
			if (col_temp != i)
			{
				out[i] -= out[col_temp] * matrix.val[j];
			}
			else
			{
				break;
			}
		}
	}

	for (int i = matrix.n_row - 1; i >= 0; i--)
	{
		int ja = matrix.row_index[i];
		int jb = matrix.row_index[i + 1];
		for (int j = jb - 1; j >= ja; j--)
		{
			int col_temp = matrix.col_index[j];
			if (col_temp != i)
			{
				out[i] -= out[col_temp] * matrix.val[j];
			}
			else
			{
				out[i] /= matrix.val[j];
				break;
			}
		}

	}
}

double* ilu0::allocation_solve(const csr_matrix& matrix, const double* b)
{
	double* rez = (double*)malloc(matrix.n_col * sizeof(double));
	solve(matrix, b, rez);
	return rez;
}

void ilu0::solve(const dcsr_matrix& matrix, const double* b, double* out)
{

	int n_thread = matrix.n_blocks;
	int* shift = (int*)malloc(n_thread * sizeof(int));

	shift[0] = 0;

	for (int i = 1; i < n_thread; i++)
	{
		shift[i] = shift[i - 1] + matrix.matrix[i - 1].n_row;
	}

#pragma omp parallel for schedule(static, 1)
	for (int n = 0; n < n_thread; n++)
	{
		double* temp_rez = allocation_solve(matrix.matrix[n], b + shift[n]);
		memcpy(out + shift[n], temp_rez, matrix.matrix[n].n_row * sizeof(double));
		free(temp_rez);
	}

	free(shift);

}

double* ilu0::allocation_solve(const dcsr_matrix& matrix, const double* b)
{
	double* rez = (double*)malloc(matrix.n_row * sizeof(double));
	solve(matrix, b, rez);
	return rez;
}

csr_matrix ilu0::get_L(const csr_matrix& matrix)
{
	csr_matrix rez = csr_matrix(matrix.n_nnz, matrix.n_row, matrix.n_col);

	//fill L
	int iter = 0;
	for (int i = 0; i < matrix.n_row; i++)
	{
		int strt = matrix.row_index[i];
		int end = matrix.row_index[i + 1];
		for (int j = strt; j < end; j++)
		{
			int col_temp = matrix.col_index[j];
			if (col_temp == i) break;
			rez.val[iter] = matrix.val[j];
			rez.col_index[iter] = col_temp;
			iter++;
		}
		rez.val[iter] = 1.;
		rez.col_index[iter] = i;
		iter++;
		rez.row_index[i + 1] = iter;
	}
	rez.n_nnz = iter;
	if (iter != matrix.n_nnz)
	{
		rez.val = (double*)realloc(rez.val, iter * sizeof(double));
		rez.col_index = (int*)realloc(rez.col_index, iter * sizeof(int));

	}
	return rez;


}
csr_matrix ilu0::get_U(const csr_matrix& matrix)
{
	csr_matrix rez = csr_matrix(matrix.n_nnz, matrix.n_row, matrix.n_col);

	//fill U
	int iter = 0;
	for (int i = 0; i < matrix.n_row; i++)
	{
		int strt = matrix.row_index[i];
		int end = matrix.row_index[i + 1];
		for (int j = strt; j < end; j++)
		{
			int col_temp = matrix.col_index[j];
			if (col_temp < i) continue;
			rez.val[iter] = matrix.val[j];
			rez.col_index[iter] = col_temp;
			iter++;
		}
		rez.row_index[i + 1] = iter;
	}
	rez.n_nnz = iter;
	if (iter != matrix.n_nnz)
	{
		rez.val = (double*)realloc(rez.val, iter * sizeof(double));
		rez.col_index = (int*)realloc(rez.col_index, iter * sizeof(int));

	}
	return rez;


}


dcsr_matrix ilu0::get_L(const dcsr_matrix& matrix)
{
	int n_thread = matrix.n_blocks;

	dcsr_matrix rez = dcsr_matrix(matrix.n_row, matrix.n_col, n_thread);


#pragma omp parallel for schedule(dynamic, 1) 
	for (int i = 0; i < n_thread; i++)
	{
		rez.matrix[i] = get_L(matrix.matrix[i]);
	}

	return rez;

}
dcsr_matrix ilu0::get_U(const dcsr_matrix& matrix)
{
	int n_thread = matrix.n_blocks;

	dcsr_matrix rez = dcsr_matrix(matrix.n_row, matrix.n_col, n_thread);


#pragma omp parallel for schedule(dynamic, 1) 
	for (int i = 0; i < n_thread; i++)
	{
		rez.matrix[i] = get_U(matrix.matrix[i]);
	}

	return rez;
}