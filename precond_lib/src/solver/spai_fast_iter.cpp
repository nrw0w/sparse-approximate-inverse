#include "solver/spai_fast_iter.h"

void spai_fast_iter::diogonal_block_decompose(const csr_matrix& input_matrix) 
{
	int n_row = input_matrix.getRows();
	int n_col = input_matrix.getCols();
	if (n_row != n_col)
	{
#ifdef __linux__ 
		throw std::exception();
#elif _WIN32
		throw std::exception("Matrices are not square");
#endif
	}

	p = dcsr_matrix(n_row, n_col, n_threads);
	r = dcsr_matrix(n_row, n_col, n_threads);
	//p.matrix = std::vector<csr_matrix>(n_threads);
	//r.matrix = std::vector<csr_matrix>(n_threads);

	int remainder = n_row % n_threads;
	int n_chunk = n_row / n_threads;

	int* first_elem_in_inp_matrix = (int*)calloc(n_threads + 1, sizeof(int));
	int* size_of_block = (int*)malloc(n_threads * sizeof(int));

	for (int i = 1; i < n_threads + 1; i++)
	{
		if (remainder > 0)
		{
			size_of_block[i - 1] = n_chunk + 1;
			first_elem_in_inp_matrix[i] = first_elem_in_inp_matrix[i - 1] + n_chunk + 1;
			remainder--;
		}
		else
		{
			size_of_block[i - 1] = n_chunk;
			first_elem_in_inp_matrix[i] = first_elem_in_inp_matrix[i - 1] + n_chunk;
		}
	}
#pragma omp parallel for schedule(static,1)
	for (int n = 0; n < n_threads; n++)
	{
		int start_local = first_elem_in_inp_matrix[n];
		int end_local = first_elem_in_inp_matrix[n + 1];

		int n_nnz_max = input_matrix.row_index[end_local] - input_matrix.row_index[start_local];

		p.matrix[n] = csr_matrix(n_nnz_max, size_of_block[n], size_of_block[n]);
		
		r.matrix[n] = csr_matrix(n_nnz_max, size_of_block[n], n_col);

		int n_nnz_p = 0;
		int n_nnz_r = 0;
		for (int i = start_local; i < end_local; i++)
		{
			int ja = input_matrix.row_index[i];
			int jb = input_matrix.row_index[i + 1];
			//#pragma omp simd
			for (int j = ja; j < jb; j++)
			{
				int temp_col_index = input_matrix.col_index[j];
				if (temp_col_index >= start_local && temp_col_index < end_local)
				{
					p.matrix[n].val[n_nnz_p] = input_matrix.val[j];
					p.matrix[n].col_index[n_nnz_p] = temp_col_index - start_local;
					n_nnz_p++;
				}
				else
				{
					r.matrix[n].val[n_nnz_r] = input_matrix.val[j];
					r.matrix[n].col_index[n_nnz_r] = temp_col_index;
					n_nnz_r++;
				}
			}
			p.matrix[n].row_index[i - start_local + 1] = n_nnz_p;
			r.matrix[n].row_index[i - start_local + 1] = n_nnz_r;
		}
		if (n_nnz_p != p.matrix[n].n_nnz)
		{
			p.matrix[n].n_nnz = n_nnz_p;
			p.matrix[n].val = (double*)realloc(p.matrix[n].val, p.matrix[n].n_nnz * sizeof(double));
			p.matrix[n].col_index = (int*)realloc(p.matrix[n].col_index, p.matrix[n].n_nnz * sizeof(int));
		}
		if (n_nnz_r != r.matrix[n].n_nnz)
		{
			r.matrix[n].n_nnz = n_nnz_r;
			r.matrix[n].val = (double*)realloc(r.matrix[n].val, r.matrix[n].n_nnz * sizeof(double));
			r.matrix[n].col_index = (int*)realloc(r.matrix[n].col_index, r.matrix[n].n_nnz * sizeof(int));
		}
	}
	free(first_elem_in_inp_matrix);
	free(size_of_block);
}


spai_fast_iter::spai_fast_iter(const csr_matrix& inp_matrix, int n_iterations, int n_threads) : spai_base(inp_matrix, n_iterations, n_threads)
{

	double time = omp_get_wtime();
	diogonal_block_decompose(inp_matrix);
	time_to_decompose = omp_get_wtime() - time;


	time = omp_get_wtime();
	p = ilu0::decompose(p);
	time_to_ilu = omp_get_wtime() - time;
}

void spai_fast_iter::solve(const double* b, double* out)
{
	int n_row = r.getRows();
	//�������
	if (!prec)
	{
		memcpy(out, b, n_row * sizeof(double));
		return;
	}

	memset(out, 0, n_row * sizeof(double));

	double* temp_sol = (double*)malloc(n_row * sizeof(double));
	double* temp_mult = (double*)malloc(n_row * sizeof(double));
	memcpy(temp_mult, b, n_row * sizeof(double));

	int sign = 1;

	for (int i = 0; i <= n_iterations; i++)
	{
		double time = omp_get_wtime();
		ilu0::solve(p, temp_mult, temp_sol);
		time_to_sol += omp_get_wtime() - time;
//#pragma omp parallel for schedule(auto)
#pragma omp parallel for schedule(guided)
		for (int j = 0; j < n_row; j++)
		{
			out[j] += sign * temp_sol[j];
		}
		if (i == n_iterations)
		{
			break;
		}
		time = omp_get_wtime();
		r.spmv(temp_sol, temp_mult);
		time_to_spmv += omp_get_wtime() - time;
		sign *= -1;
	}
	free(temp_sol);
	free(temp_mult);

}