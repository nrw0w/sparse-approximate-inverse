#include "solver/bicgstab.h"
#include <solver/spai_fast_init.h>

void bicgstab::setMaxIterations(int n_max_iter)
{
	if (n_max_iter <= 0)
	{
		throw std::exception();
	}
	this->n_max_iter = n_max_iter;
}

int bicgstab::getMaxIterations()
{
	return n_max_iter;
}

void bicgstab::setTolerance(double tol)
{
	if (tol < 0)
	{
		throw std::exception();

	}
	this->tol = tol;
}

double bicgstab::getTolerance()
{
	return tol;
}
double bicgstab::getError()
{
	return error;
}

bicgstab::bicgstab()
{
	
}


bicgstab::bicgstab(double tol, int n_max_iter)
{
	this->tol = tol;
	this->n_max_iter = n_max_iter;
}

void bicgstab::solve(const csr_matrix& A, const  double* rhs, spai_base& precond, double* out)
{
	int n_col = A.getCols();
	int n_row = A.getRows();
	double time = omp_get_wtime();
	double alpha = 1.,
		omega = 1.,
		rho_curr = 1.,
		norm_rhs = 0.,
		norm_r0 = 0.;

	double* v = (double*)calloc(n_col, sizeof(double));
	double* p = (double*)calloc(n_col, sizeof(double));

	double* s = (double*)malloc(n_col * sizeof(double));
	double* t = (double*)malloc(n_col * sizeof(double));

	double* r_0 = (double*)malloc(n_col * sizeof(double));

	double* y = (double*)malloc(n_col * sizeof(double));
	double* z = (double*)malloc(n_col * sizeof(double));

	memset(out, 0, n_col * sizeof(double));

	double* r = A * out;


	for (int i = 0; i < n_row; i++)
	{
		r[i] = rhs[i] - r[i];
		norm_rhs += rhs[i] * rhs[i];
		norm_r0 += r[i] * r[i];
	}
	memcpy(r_0, r, n_col * sizeof(double));

	if (norm_rhs == 0)
	{
		free(p);
		free(s);
		free(r);
		free(r_0);
		free(t);
		free(v);
		free(z);
		free(y);
		return;
	}

	constexpr double eps2 = std::numeric_limits<double>::epsilon() * std::numeric_limits<double>::epsilon();
	double tol2 = tol * tol * norm_rhs;
	double temp_err = 0;
	int restarts = 0;
	for (int k = 1;; k++)
	{
		double rho_prev = rho_curr;
		rho_curr = 0;
		for (int i = 0; i < n_col; i++)
		{
			rho_curr += r[i] * r_0[i];
		}

		//---украдено
		if (abs(rho_curr) < eps2 * norm_r0)
		{
			A.parallel_spmv(out, r);
			for (int i = 0; i < n_row; i++)
			{
				r[i] = rhs[i] - r[i];
				norm_r0 += r[i] * r[i];
			}
			memcpy(r_0, r, n_col * sizeof(double));
			rho_curr = norm_r0;
			if (restarts++ == 0) k = 1;
		}
		//---

		double beta = (rho_curr / rho_prev) * (alpha / omega);

		for (int i = 0; i < n_col; i++)
		{
			p[i] = r[i] + beta * (p[i] - omega * v[i]);
		}

		double temp_time1 = omp_get_wtime();
		precond.solve(p, y);
		double temp_time2 = omp_get_wtime();

		time_prec += temp_time2 - temp_time1;

		double temp_time3 = omp_get_wtime();
		A.parallel_spmv(y, v);
		double temp_time4 = omp_get_wtime();

		time_spmv += temp_time4 - temp_time3;


		double temp = 0;
		for (int i = 0; i < n_col; i++)
		{
			temp += r_0[i] * v[i];
		}

		alpha = rho_curr / temp;

		for (int i = 0; i < n_col; i++)
		{
			s[i] = r[i] - alpha * v[i];
		}

		double temp_time5 = omp_get_wtime();
		precond.solve(s, z);
		double temp_time6 = omp_get_wtime();

		time_prec += temp_time6 - temp_time5;

		double temp_time7 = omp_get_wtime();
		A.parallel_spmv(z, t);
		double temp_time8 = omp_get_wtime();

		time_spmv += temp_time8 - temp_time7;

		//---украдено
		temp = 0;
		omega = 0;
		for (int i = 0; i < n_col; i++)
		{
			temp += t[i] * t[i];
		}
		if (temp > std::numeric_limits<double>::epsilon())
		{
			for (int i = 0; i < n_col; i++)
			{
				omega += t[i] * s[i];
			}
			omega /= temp;

		}
		//---

		temp_err = 0;

		for (int i = 0; i < n_col; i++)
		{
			out[i] += omega * z[i] + alpha * y[i];
			r[i] = s[i] - omega * t[i];
			temp_err += r[i] * r[i];
		}

		if (temp_err <= tol2 || k>=n_max_iter)
		{
			iter_to_solve = k;
			break;
		}

	}
	error = sqrt(temp_err / norm_rhs);

	free(p);
	free(s);
	free(r);
	free(r_0);
	free(t);
	free(v);
	free(z);
	free(y);
	time_to_solve = omp_get_wtime() - time;
}

double* bicgstab::allocation_solve(const csr_matrix& A, const double* rhs, spai_base& precond)
{
	double* out = (double*)malloc(A.getCols() * sizeof(double));
	solve(A, rhs, precond, out);
	return out;
}

void bicgstab::solve(const csr_matrix& A, const  double* rhs, double* out)
{
	spai_fast_init precond(A, false);
	solve(A, rhs, precond, out);
}

double* bicgstab::allocation_solve(const csr_matrix& A, const double* rhs)
{
	double* out = (double*)malloc(A.getCols() * sizeof(double));
	solve(A, rhs, out);
	return out;
}

void bicgstab::stat()
{
	std::cout << "time to solve(bicgstab): " << time_to_solve << std::endl << "iteration: " << iter_to_solve << std::endl;

}

double bicgstab::getTime()
{
	return time_to_solve;
}

int bicgstab::getIterations()
{
	return iter_to_solve;
}
