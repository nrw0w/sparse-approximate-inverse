#include <gtest/gtest.h>
#include <matrix/csr_matrix.h>
#include <Eigen/Sparse>
#include <unsupported/Eigen/SparseExtra>



class TestCsrMatrix : public ::testing::Test
{
protected:
	void SetUp()
	{
		std::string test1 = "C:\\Users\\naryWoW\\source\\kursovaya\\precond\\precond_lib\\test\\testObj\\bcsstk18.mtx";
		testMatrix1 = csr_matrix(test1);
		Eigen::loadMarket(refMatrix1, test1);
		b1 = new double[testMatrix1.getRows()];
		rez1 = new double[testMatrix1.getRows()];

		refb1 = Eigen::VectorXd(testMatrix1.getRows());
		refb1.setRandom();
		for (int i = 0; i < testMatrix1.getRows(); i++)
		{
			b1[i] = refb1.data()[i];
		}
	}
	void TearDown()
	{
		delete[] b1;
		delete[] rez1;

	}
	static int a;
	csr_matrix testMatrix1;
	double* b1;
	double* rez1;

	Eigen::VectorXd refb1;
	Eigen::SparseMatrix<double> refMatrix1;
};


TEST_F(TestCsrMatrix, test_spmv) 
{
	testMatrix1.spmv(b1, rez1);

	Eigen::VectorXd refrez1;
	refrez1 = refMatrix1 * refb1;

	for (int i = 0; i < testMatrix1.getRows(); i++)
	{
		ASSERT_DOUBLE_EQ(rez1[i], refrez1.data()[i]);
	}

}
TEST_F(TestCsrMatrix, test_parallel_spmv) 
{
	
	Eigen::VectorXd refrez1;
	refrez1 = refMatrix1 * refb1;

	for (int th = 1; th < omp_get_num_procs(); th++)
	{
		omp_set_num_threads(th);
		testMatrix1.parallel_spmv(b1, rez1);

		for (int i = 0; i < testMatrix1.getRows(); i++)
		{
			ASSERT_DOUBLE_EQ(rez1[i], refrez1.data()[i]);
		}
	}

}
