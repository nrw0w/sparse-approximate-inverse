#pragma once

#ifdef __linux__ 
#include <cstring>
#elif _WIN32
#include <string>
#endif

#include "matrix/matrix_base.h"
#include "matrix/dcsr_matrix.h"
#include <cstdlib>
#include <time.h>
#include <fstream>
#include <iostream>
#include <exception>
#include <vector>
#include <omp.h>
#include <sstream>
#include <limits>

class dcsr_matrix;

class csr_matrix :
    public matrix_base
{
	friend class dcsr_matrix;
	friend class ilu0;

public:
	csr_matrix();

	csr_matrix(const csr_matrix&);
	//convert matrix in one-dimensional array to CSR format
	csr_matrix(const double* array, const int n_row, const int n_col);

	csr_matrix(std::string filename);

	csr_matrix(int n_nnz, int n_row, int n_col);

	~csr_matrix();

	void transpose();

	//multiplicate CSR format matrix with vector
	double* operator*(const double* vector) const;

	void spmv(const double* vector, double* out) const;

	void parallel_spmv(const double* vector, double* out) const;

	//multiplicate CSR format matrix with CSR format matrix
	csr_matrix operator*(const csr_matrix& matrix) const;

	csr_matrix operator*(const double scalar) const;
	
	csr_matrix operator*(const int scalar) const;

	csr_matrix operator-(const csr_matrix& matrix) const;

	csr_matrix operator=(const csr_matrix& matrix);

	void diogonal_block_decompose(int n_blocks, dcsr_matrix& p, dcsr_matrix& r) const;

	//convert CSR matrix to one-dimensional array
	double* allocate_convert_to_array() const;

	//generate random CSR matrix witn n_nnz_in_row nonzero element in row
	void random_generate(const int n_row, const int n_col, const int n_nnz_in_row);

//private:

	//initialization csr 
	void init_csr(const int n_row, const int n_col, const int allocation_chunk);

	int n_nnz = 0;	//nonzero elements count
	double* val = nullptr;	//array with nonzero elements
	int* col_index = nullptr;	//array with column index of nonzero element
	int* row_index = nullptr;	/*array with count nonzero elements from
			first row to i'st row. First element of array is 0*/

	

};

