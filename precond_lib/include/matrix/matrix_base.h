#pragma once

class matrix_base
{
public:
	int getRows() const
	{
		return n_row;
	}
	int getCols() const
	{
		return n_col;
	}

protected:
	int n_row = 0;
	int n_col = 0;

};

