#pragma once

#ifdef __linux__ 
#include <cstring>
#elif _WIN32
#include <string>
#endif

#include "matrix/csr_matrix.h"
#include "matrix/matrix_base.h"
#include <cstdlib>
#include <time.h>
#include <fstream>
#include <iostream>
#include <exception>
#include <vector>
#include <omp.h>

class csr_matrix;

class dcsr_matrix :
	public matrix_base
{
	friend class csr_matrix;
	friend class ilu0;
public:

	dcsr_matrix();
	dcsr_matrix(const dcsr_matrix&);
	dcsr_matrix(int n_row, int n_col, int n_blocks);
	dcsr_matrix(const csr_matrix input_matrix, int n_blocks);
	~dcsr_matrix();

	dcsr_matrix operator*(const dcsr_matrix& matrix) const;
	dcsr_matrix dcsr_matrix::operator=(const dcsr_matrix& matrix);
	void spmv(const double* rhs, double* out);
	csr_matrix to_csr() const;

//protected:
	//dyn array
	//csr_matrix* matrix = nullptr;	//array with matrix
	//vector
	std::vector<csr_matrix> matrix;	//array with matrix
	int n_blocks;	//number of blocks
};