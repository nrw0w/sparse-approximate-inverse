#pragma once

#include "solver/spai_base.h"

class spai_fast_iter :
	public spai_base
{
public:
	
	spai_fast_iter(const csr_matrix& inp_matrix, int n_iterations, int n_threads);
	void solve(const double* b, double* out) override;
private:

	void diogonal_block_decompose(const csr_matrix& input_matrix) override;
	dcsr_matrix r;

};
