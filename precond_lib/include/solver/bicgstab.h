#pragma once
#include "solver/solver_base.h"
#include "solver/spai_base.h"
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <omp.h>
#include <limits>


class bicgstab
{
public:
    bicgstab();
    bicgstab(double tol, int n_max_iter);

    void setMaxIterations(int n_max_iter);
    int getMaxIterations();
    void setTolerance(double tol);
    double getTolerance();
    double getError();
  
    void solve(const csr_matrix& A, const double* rhs, spai_base& precond, double* out);
    double* allocation_solve(const csr_matrix& A,const double* rhs, spai_base& precond);
    void solve(const csr_matrix& A,const double* rhs, double* out);
    double* allocation_solve(const csr_matrix& A,const double* rhs);

    double getTime();
    int getIterations();
    void stat();

    double time_prec=0.;
    double time_spmv=0.;

private:
    double tol = 1e-9;
    int n_max_iter = 1000;
    int iter_to_solve = 0;
    double time_to_solve = 0.;
    double error = 0.;
};

