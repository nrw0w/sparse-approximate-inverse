#pragma once

#include "matrix/dcsr_matrix.h"
#include "matrix/csr_matrix.h"
#include "solver/solver_base.h"
#include "solver/ilu0.h"
class csr_matrix;

class spai_base
{
public:

	//�������
	spai_base(const csr_matrix& inp_matrix, bool prec);

	spai_base(const csr_matrix& inp_matrix, int n_iterations, int n_threads);

	void setIterations(int n_iterations);
	int getIterations();
	void setThreads(int n_threads);
	int getThreads();
	
	virtual void solve(const double* b, double* out) = 0;
	double* allocation_solve(const double* b);


	double time_to_decompose = 0.;
	double time_to_ilu = 0.;
	double time_to_sol = 0.;
	double time_to_spmv = 0.;

protected:

	virtual void diogonal_block_decompose(const csr_matrix& input_matrix) = 0;
	//�������
	bool prec = false;

	int n_iterations;
	int n_threads;
	dcsr_matrix p;
	
};