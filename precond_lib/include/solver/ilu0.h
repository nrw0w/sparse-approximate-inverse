#pragma once
#include "matrix/dcsr_matrix.h"
#include "matrix/csr_matrix.h"
#include <cstring>
#include <stdlib.h>
#include <cmath>
#include <limits>

class dcsr_matrix;

class ilu0
{
public:

    static csr_matrix decompose(const csr_matrix & matrix);
    static dcsr_matrix decompose(const dcsr_matrix & matrix);

    static void solve(const csr_matrix& matrix, const double* b, double* out);
    static double* allocation_solve(const csr_matrix& matrix, const double* b);

    static void solve(const dcsr_matrix& matrix, const double* b, double* out);
    static double* allocation_solve(const dcsr_matrix& matrix, const double* b);

    static csr_matrix get_L(const csr_matrix& matrix);
    static csr_matrix get_U(const csr_matrix& matrix);

    static dcsr_matrix get_L(const dcsr_matrix& matrix);
    static dcsr_matrix get_U(const dcsr_matrix& matrix);


};

