#pragma once
#include "matrix/matrix_base.h"

class solver_base
{
public:

	virtual void solve(const matrix_base& A,const double* rhs, double* out) = 0;
};

