#pragma once

#include "solver/spai_base.h"
//#include "matrix/csr_matrix.h"
//class csr_matrix;

class spai_fast_init :
	public spai_base
{
public:
	spai_fast_init(const csr_matrix& inp_matrix, bool prec);
	spai_fast_init(const csr_matrix& inp_matrix, int n_iterations, int n_threads);
	~spai_fast_init();
	void solve(const double* b, double* out) override;

public:

	int bcr(int n, int k);
	void diogonal_block_decompose(const csr_matrix& input_matrix) override;


	csr_matrix a;
	int* bcr_cache = nullptr;
};
