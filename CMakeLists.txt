﻿
cmake_minimum_required (VERSION 3.8)

project ("precond")

add_subdirectory("precond_lib")
add_executable (precond "precond/precond.cpp")
target_link_libraries(precond PUBLIC precond_library)
